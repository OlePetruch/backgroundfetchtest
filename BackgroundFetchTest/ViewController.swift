//
//  ViewController.swift
//  BackgroundFetchTest
//
//  Created by Петрічук Олег Аркадійовіч on 01.03.18.
//  Copyright © 2018 Oleg Petrychuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let data: [String] = UserDefaults.standard.object(forKey: "Key") as? [String] ?? [String]()
        if data.isEmpty {
            textView.text = "Empty"
        } else {
            var str = ""
            data.forEach({ dataStr in
                str = str.appending("\(dataStr)\n")
            })
            textView.text = str
        }
    }
}

