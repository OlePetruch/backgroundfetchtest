//
//  AppDelegate.swift
//  BackgroundFetchTest
//
//  Created by Петрічук Олег Аркадійовіч on 01.03.18.
//  Copyright © 2018 Oleg Petrychuk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        addData()
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func addData() {
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm"
        let newData = dateFormatter.string(from:date as Date)
        
        var data: [String] = UserDefaults.standard.object(forKey: "Key") as? [String] ?? [String]()
        data.append(newData)
        UserDefaults.standard.set(data, forKey: "Key")
    }
}

